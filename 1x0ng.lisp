(in-package :1x0ng)

(defparameter *1x0ng-copyright-notice*
"
  #            ###                 
 ##   #    #  #   #  #    #  ####  
# #    #  #  #   # # ##   # #    # 
  #     ##   #  #  # # #  # #      
  #     ##   # #   # #  # # #  ### 
  #    #  #   #   #  #   ## #    # 
##### #    #   ###   #    #  ####  
-----------------------------------------------------------------
Welcome to 1x0ng. 
1x0ng and Xelf are Copyright (C) 2006-2016 by David T. O'Toole 
email: <dto@xelf.me>   website: http://xelf.me/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Full license text of the GNU Lesser General Public License is in the
enclosed file named 'COPYING'. Full license texts for compilers,
assets, libraries, and other items are contained in the LICENSES
directory included with this application.
-----------------------------------------------------------------
")

(defvar *coop-p* nil)
(defun coop-p () *coop-p*)

(defresource "vixon.ogg" :volume 100)
(defresource "activate.wav" :volume 60)
(defresource "fanfare-1.wav" :volume 120)
(defresource "fanfare-2.wav" :volume 120)
(defresource "fanfare-3.wav" :volume 120)
(defresource "bip.wav" :volume 70)
(defresource "bleep.wav" :volume 70)
(defresource "bloop.wav" :volume 70)
(defresource "bounce.wav" :volume 70)
(defresource "buzz.wav" :volume 30)
(defresource "chevron.wav" :volume 70)
(defresource "death-alien.wav" :volume 100)
(defresource "death.wav" :volume 70)
(defresource "error-old1.wav" :volume 70)
(defresource "error.wav" :volume 70)
(defresource "freeze.wav" :volume 70)
(defresource "gate-closing-sound.wav" :volume 70)
(defresource "go.wav" :volume 70)
(defresource "grab.wav" :volume 70)
(defresource "hole.wav" :volume 70)
(defresource "lock-opening-sound.wav" :volume 70)
(defresource "muon-fire.wav" :volume 70)
(defresource "phasebell.wav" :volume 70)
(defresource "plasma.wav" :volume 70)
(defresource "powerup.wav" :volume 70)
(defresource "serve.wav" :volume 70)
(defresource "shield-warning.wav" :volume 170)
(defresource "shield.wav" :volume 170)
(defresource "talk.wav" :volume 70)
(defresource "turn.wav" :volume 70)
(defresource "upwoop.wav" :volume 70)
(defresource "worp.wav" :volume 70)
(defresource "xeng.wav" :volume 70)
(defresource "zap1.wav" :volume 70)
(defresource "zap2.wav" :volume 70)
(defresource "zap3.wav" :volume 70)
(defresource "zap4.wav" :volume 70)
(defresource "zap5.wav" :volume 70)
(defresource "zap6.wav" :volume 70)
(defresource "zap7.wav" :volume 70)

(defparameter *paused* nil)
(defparameter *soundtrack* nil)
(defparameter *grid-directions* '(:right :up :left :down))
(defparameter *grid-diagonals* '(:upright :upleft :downright :downleft))

;;; Controls

(defvar *player-1-joystick* 0)
(defvar *player-2-joystick* nil)

(defparameter *player-1-config* 
  '(:throw 14 :deploy 15 :left 7 :right 5 :up 4 :down 6))

(defparameter *player-2-config*
  '(:throw 14 :deploy 15 :left 7 :right 5 :up 4 :down 6))

(defun holding-button (keyword &key (joystick *player-1-joystick*) (config *player-1-config*))
  (when (numberp joystick)
    (let ((button (getf config keyword)))
      (if button
	  (joystick-button-pressed-p button joystick)))))

(defun holding-down-arrow () (or (keyboard-down-p :kp2) (keyboard-down-p :down)))
(defun holding-up-arrow () (or (keyboard-down-p :kp8) (keyboard-down-p :up)))
(defun holding-left-arrow () (or (keyboard-down-p :kp4) (keyboard-down-p :left)))
(defun holding-right-arrow () (or (keyboard-down-p :kp6) (keyboard-down-p :right)))
(defun holding-space () (keyboard-down-p :space))     
(defun holding-return () (or (keyboard-down-p :kp-enter)
			     (keyboard-down-p :return)))

;;; Game clock and scoring

(defparameter *score-font* "sans-mono-bold-12")
(defparameter *big-font* "sans-mono-bold-16")

(defun seconds (n) (* 60 n))
(defun minutes (n) (* (seconds 60) n))

(defparameter *game-length* (minutes 3))

(defvar *game-clock* 0)

(defun reset-game-clock ()
  (setf *game-clock* 0))

(defun update-game-clock ()
  (incf *game-clock*))

(defun game-on-p () (plusp *game-clock*))

(defun game-clock () *game-clock*)

(defun game-clock-string (&optional (clock *game-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

(defvar *score-1* 0)
(defvar *score-2* 0)

(defun reset-score () (setf *score-1* 0 *score-2* 0))

;;; Utility functions

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

;;; Buffer utils (see below defs for Arena)

(defmethod grid-offset ((buffer buffer)) 0)
(defmethod grid-object-size ((buffer buffer)) (units 1))

;;; Main objects 

(defvar *arena* nil)
(defun arena () *arena*)
(defvar *player-1* nil)
(defvar *player-2* nil)
(defvar *goal-1* nil)
(defvar *goal-2* nil)
(defun player-1 () *player-1*)
(defun player-2 () *player-2*)
(defun set-player-1 (x) (setf *player-1* x))
(defun set-player-2 (x) (setf *player-2* x))
(defun player-1-p (x) (eq (find-object x) (find-object *player-1*)))
(defun player-2-p (x) (eq (find-object x) (find-object *player-2*)))

(defvar *ball-1* nil)
(defvar *ball-2* nil)
(defun ball-1 () *ball-1*)
(defun ball-2 () *ball-2*)
(defun set-ball-1 (x) (setf *ball-1* x))
(defun set-ball-2 (x) (setf *ball-2* x))

(defparameter *width* 1280)
(defparameter *height* 720)

(defparameter *player-1-color* "cyan")
(defparameter *player-2-color* "hot pink")
(defparameter *neutral-color* "white")
(defparameter *arena-color* "yellow green")

;;; Defining the grid-based world

(setf xelf:*unit* 17)

(defun grid-position (x y)
  (values (units x) (units y)))

(defun grid-position-center (x y)
  (values (cfloat (+ (units x) (units 0.5)))
	  (cfloat (+ (units y) (units 0.5)))))

(defun grid-position-bounding-box (x y)
  ;; top left right bottom
  (values (units y) 
	  (units x) 
	  (units (1+ x)) 
	  (units (1+ y))))

(defparameter *margin-width* 0.5)

(defun grid-object-bounding-box (x y)
  (values (+ (units y) *margin-width*)
	  (+ (units x) *margin-width*)
	  (- (units (1+ x)) *margin-width*)
	  (- (units (1+ y)) *margin-width*)))

;;; Level progression and color themes

(defparameter *level* 0)

(defun by-level (&rest args)
  (if (<= (length args) *level*)
      (nth (- (length args) 1) args)
      (nth *level* args)))

(defparameter *difficulty* 0)

(defun with-difficulty (&rest args)
  (if (<= (length args) *difficulty*)
      (nth (1- (length args)) args)
      (nth *difficulty* args)))

(defparameter *depth* 0)

(defun with-depth (&rest args)
  (if (<= (length args) *depth*)
      (nth (1- (length args)) args)
      (nth *depth* args)))

(defparameter *two-brick-themes* 
  '((:snefru "DarkSlateBlue" "green" 
     "magenta" "cyan")
    (:xalcrys "black" "blue violet" 
     "deep pink" "cornflower blue")
    (:zupro "olive drab" "hot pink" 
     "cyan" "yellow")))

(defparameter *three-brick-themes*
  '((:snafu "dark magenta" "gray20" 
     "cyan" "red" "yellow")
    (:atlantis "midnight blue" "dodger blue" 
     "pale green" "hot pink" "red")
    (:radium "dark olive green" "gold"
     "cyan" "chartreuse" "magenta")
    (:krez "dark orchid" "maroon2" 
     "green" "red" "yellow")))

(defparameter *four-brick-themes*
  '((:zerk "black" ;; "gray40" 
     "maroon2" "yellow green" "orange" "cornflower blue")
    (:tandy "DarkSlateBlue" ;; "gray80" 
     "yellow" "green" "cyan" "deep pink")
    (:zendium "gray17" ;; "orchid"
     "deep pink" "deep sky blue" "chartreuse" "orange")
    (:command "dim gray" ;; "yellow" 
     "cyan" "deep pink" "red" "green yellow")))

(defvar *red-green-color-blindness* nil)

(defparameter *red-green-color-blindness-theme* 
  '("cornflower blue" "yellow" "pale green" "violet red"))

(defun red-green-color-blindness-theme (&optional (colors 4))
  (append (list "black" "gray50")
	  (subseq (derange *red-green-color-blindness-theme*) 
		  0 colors)))

(defresource "hash1.png")
(defresource "hash2.png")
(defresource "hash3.png")
(defresource "hash4.png")

(defun ball-hash-image (n)
  (ecase n
    (1 "hash1.png")
    (2 "hash2.png")
    (3 "hash3.png")
    (4 "hash4.png")
    (5 "hash1.png")
    (6 "hash2.png")
    (7 "hash3.png")
    (8 "hash4.png")))

(defresource "gate1-hash1.png")
(defresource "gate1-hash2.png")
(defresource "gate1-hash3.png")
(defresource "gate1-hash4.png")
(defresource "gate2-hash1.png")
(defresource "gate2-hash2.png")
(defresource "gate2-hash3.png")
(defresource "gate2-hash4.png")
(defresource "brick-hash1.png")
(defresource "brick-hash2.png")
(defresource "brick-hash3.png")
(defresource "brick-hash4.png")

(defun hash-image (n)
  (if *red-green-color-blindness*
      (ecase n
	(1 "gate1-hash1.png")
	(2 "gate1-hash2.png")
	(3 "gate1-hash3.png")
	(4 "gate1-hash4.png"))
      "gate.png"))

(defun large-hash-image (n)
  (if *red-green-color-blindness*
      (ecase n
	(1 "gate2-hash1.png")
	(2 "gate2-hash2.png")
	(3 "gate2-hash3.png")
	(4 "gate2-hash4.png"))
      "gate2.png"))

(defun solid-hash-image (n)
  (or (when *red-green-color-blindness*
	(case n
	  (1 "brick-hash1.png")
	  (2 "brick-hash2.png")
	  (3 "brick-hash3.png")
	  (4 "brick-hash4.png")))
      "gate3.png"))

(defparameter *boss-theme* '(:voltz "black" "gray30" "orchid" "medium orchid" "dark orchid" "deep pink" "green yellow"))

(defparameter *themes* 
  (append *two-brick-themes* *three-brick-themes*
	  *four-brick-themes* (list *boss-theme*)))

(defun find-theme (name)
  (rest (assoc name *themes*)))

(defparameter *theme* '("black" "white" "red" "blue"))

(defun theme-colors (&optional (theme *theme*))
  (rest (rest theme)))

(defun color-hash (color)
  (let ((pos (position color (theme-colors) :test 'equal)))
    (when pos (1+ pos))))

(defun set-theme (&optional (theme :wizard))
  (setf *theme* 
	(if (consp theme) 
	    theme
	    (find-theme theme))))

(defun random-theme () (random-choose (mapcar #'car *themes*)))

(defun set-random-theme () (set-theme (random-theme)))

(defun background-color ()
  (when *theme* (first *theme*)))

(defun wall-color ()
  (when *theme* (second *theme*)))

(defun level-colors ()
  (when *theme* (rest (rest *theme*))))

(defparameter *levels* 
  '((:difficulty 0 :colors 2 :hazards nil :wildcards nil)
    ;; 1
    (:difficulty 1 :colors 2 :hazards nil :wildcards nil) 
    (:difficulty 2 :colors 3 :hazards (hole paddle) :wildcards nil)
    (:difficulty 3 :colors 3 :hazards (hole hole paddle tracer) :wildcards nil)
    (:difficulty 3 :colors 3 :hazards (hole tracer paddle) :wildcards (ghost thief nil))
    ;; 5
    (:difficulty 4 :colors 3 :hazards (hole hole tracer) :wildcards (biclops))
    (:difficulty 4 :colors 3 :hazards (tracer paddle hole) :wildcards (thief ghost))
    (:difficulty 4 :colors 4 :hazards (hole hole paddle) :wildcards (wave nil))
    (:difficulty 5 :colors 3 :hazards (hole hole paddle) :wildcards (thief biclops))
    (:difficulty 5 :colors 3 :hazards (paddle tracer) :wildcards (rook))
    ;; 10
    (:difficulty 5 :colors 4 :hazards (wave paddle hole) :wildcards (wave tracer))
    (:difficulty 6 :colors 3 :hazards (paddle hole hole) :wildcards (ghost thief))
    (:difficulty 6 :colors 3 :hazards (base paddle tracer) :wildcards (ghost wave biclops))
    (:difficulty 6 :colors 4 :hazards (base paddle wave) :wildcards (rook))
    (:difficulty 6 :colors 3 :hazards (base hole wave) :wildcards nil)
    ;; 15
    (:difficulty 6 :colors 4 :hazards (paddle tracer wave) :wildcards (ghost rook))
    (:difficulty 6 :colors 5 
     :music ("xioforms.ogg"))))

(defun nth-level (level)
  (nth (mod level (length *levels*)) *levels*))

(defun level-difficulty (&optional (level *level*))
  (getf (nth-level level) :difficulty))

(defun level-music (&optional (level *level*))
  (or (getf (nth-level level) :music)
      *soundtrack*))

(defun level-theme (&optional (level *level*))
  (let ((ncolors (getf (nth-level level) :colors)))
    (random-choose 
     (mapcar #'car
	     (ecase ncolors
	       (2 *two-brick-themes*)
	       (3 *three-brick-themes*)
	       (4 *four-brick-themes*)
	       (5 '((:voltz))))))))

(defun level-hazards (&optional (level *level*))
  (getf (nth-level level) :hazards))

(defun level-wildcards (&optional (level *level*))
  (getf (nth-level level) :wildcards))

;;; Base class for objects in 1x0ng

(defparameter *slide-frames* 10)

(defclass thing (xelf:node)
  ((grid-x :initform 0 :accessor grid-x :initarg :grid-x)
   (grid-y :initform 0 :accessor grid-y :initarg :grid-y)
   (color :initform *neutral-color* :accessor color :initarg :color)
   (speed :initform 0)
   (heading :initform 0.0)
   (direction :initform :right :accessor direction :initarg :direction)
   ;; slide movement
   (gx0 :initform 0)
   (gy0 :initform 0)
   (px :initform 0)
   (py :initform 0)
   (pz :initform 0)
   (x0 :initform 0)
   (y0 :initform 0)
   (z0 :initform 0)
   (origin-gx :initform 0)
   (origin-gy :initform 0)
   (interpolation :initform :linear)
   (frames :initform 0)
   (slide-frames :initform *slide-frames*)
   (slide-timer :initform 0)))

(defmethod find-special ((thing thing)) nil)

(defmethod resize-to-grid ((thing thing))
  (resize thing (grid-object-size (current-buffer)) (grid-object-size (current-buffer))))

(defmethod initialize-instance :after ((thing thing) &key)
  (resize-to-grid thing))

(defmethod move-to-grid ((thing thing) gx gy)
  (assert (and (integerp gx) (integerp gy)))
  (with-slots (height width grid-x grid-y gx0 gy0) thing
    (multiple-value-bind (x y) (grid-position gx gy)
      (setf grid-x gx grid-y gy)
      (move-to thing 
	       (+ x (grid-offset (current-buffer)))
	       (+ y (grid-offset (current-buffer)))
	       (setf gx0 gx gy0 gy)))))

(defun place (thing x y)
  (with-buffer (current-buffer)
    (add-node (current-buffer) thing)
    (move-to-grid thing x y)))

;;; Sliding movement 

(defmethod sliding-p ((thing thing))
  (plusp (field-value 'slide-timer thing)))

(defun interpolate (px x a &optional (interpolation :linear))
  (let ((range (- px x)))
    (float 
     (+ px
	(ecase interpolation
	  (:linear (* a range))
	  (:sine (* (sin (* a (/ pi 2))) range)))))))

(defmethod slide-to ((self thing) x1 y1 &key (interpolation :linear) frames)
  (with-slots (x y px py x0 y0 interpolation slide-timer slide-frames) self
    (setf px x py y)
    (setf x0 x1 y0 y1)
    (setf interpolation interpolation)
    (let ((timer (or frames slide-frames)))
      (setf slide-timer timer frames timer))))

(defmethod slide-to-grid ((thing thing) gx gy &key (interpolation :linear) frames)
  (multiple-value-bind (cx cy) (grid-position-center gx gy)
    (with-slots (grid-x grid-y gx0 gy0 origin-gx origin-gy height width slide-frames) thing
      (setf gx0 gx gy0 gy)
      (setf origin-gx grid-x origin-gy grid-y) 
      (let ((timer (or frames slide-frames)))
	(slide-to thing (- cx (/ width 2)) (- cy (/ height 2)) :interpolation interpolation :frames timer)))))

(defmethod update-slide ((self thing))
  (with-slots (x0 y0 px py slide-frames slide-timer interpolation) self
    (unless (zerop slide-timer)
      ;; scale to [0,1]
      (let ((a (/ (- slide-timer slide-frames) slide-frames)))
	(move-to self
		 (interpolate px x0 a interpolation)
		 (interpolate py y0 a interpolation))
	(decf slide-timer)))))

(defmethod cancel-slide ((thing thing))
  (setf (slot-value thing 'slide-timer) 0))

(defmethod snap-to-grid ((thing thing))
  (with-slots (gx0 gy0) thing
    (move-to-grid thing gx0 gy0)))

(defmethod fuzz-to-grid ((thing thing))
  (with-slots (x y) thing
    (let ((tolerance 3.0)
	  (gx (round (/ x (units 1))))
	  (gy (round (/ y (units 1)))))
      (if (< tolerance (distance x y (* *unit* gx) (* *unit* gy)))
	  (message "Warning: could not fuzzy-match ~S for grid position ~S." thing (list gx gy 'from x y))
	  (move-to-grid thing gx gy)))))

(defmethod restore-from-slide ((thing thing))
  (with-slots (origin-gx origin-gy) thing
    (move-to-grid thing origin-gx origin-gy)
    (snap-to-grid thing)))

(defmethod think ((thing thing)) nil)

(defmethod should-think-p ((thing thing))
  (not (sliding-p thing)))

(defmethod arrive ((thing thing)) 
  (snap-to-grid thing)
  (when (should-think-p thing)
    (think thing)))

(defmethod update :before ((thing thing))
  (if (sliding-p thing)
      (progn (update-slide thing)
	     ;; did we arrive?
	     (when (not (sliding-p thing))
	       (with-slots (grid-x gx0 grid-y gy0) thing
		 (setf grid-x gx0 grid-y gy0)
		 (arrive thing))))
      (when (should-think-p thing)
	(think thing))))

(defmethod cursor ((thing thing)) (player-1))

(defmethod distance-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (distance cx cy px py))))

(defmethod heading-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (find-heading cx cy px py))))

(defmethod direction-to-cursor ((thing thing))
  (or (heading-direction (heading-to-cursor thing)) :right))

(defmethod update-heading ((thing thing))
  (with-slots (heading direction) thing
    (setf heading (direction-heading direction))))

(defmethod face ((thing thing) direction)
  (setf (direction thing) direction)
  (update-heading thing))

(defmethod move-grid ((thing thing) direction &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (move-to-grid thing x y))))

(defmethod slide ((thing thing) direction &optional (distance 1) frames)
  (with-slots (grid-x grid-y slide-frames) thing
    (assert (integerp slide-frames))
    (assert (keywordp direction))
    (assert (integerp distance))
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (slide-to-grid thing x y :frames (or frames slide-frames)))))

(defmethod move-forward ((thing thing) &optional (distance 1))
  (with-slots (direction) thing
    (move-grid thing direction distance)))

(defmethod slide-forward ((thing thing) &optional (distance 1))
  (with-slots (direction slide-frames) thing
    (assert (integerp slide-frames))
    (assert (keywordp direction))
    (assert (integerp distance))
    (slide thing direction distance slide-frames)))

(defmethod turn-around ((thing thing))
  (face thing (opposite-direction (direction thing))))

(defmethod turn-leftward ((thing thing))
  (face thing (xelf::left-turn (xelf::left-turn (direction thing)))))

(defmethod turn-rightward ((thing thing))
  (face thing (xelf::right-turn (xelf::right-turn (direction thing)))))

(defmethod find-color ((thing thing))
  (slot-value thing 'color))

(defmethod paint ((thing thing) color)
  (setf (slot-value thing 'color) color))

(defun same-color-p (a b)
  (string= (find-color a) (find-color b)))

(defmethod draw ((self thing))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 ;; apply shading
				 :vertex-color color
				 :blend :alpha
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading))))))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod center-of-arena ()
  (values (/ *width* 2) (/ *height* 2)))

(defmethod heading-to-center ((thing thing))
  (multiple-value-bind (tx ty) (center-point thing)
    (multiple-value-bind (cx cy) (center-of-arena)
      (find-heading tx ty cx cy))))
  
(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun decay (x)
  (let ((z (* 0.94 x)))
    z))

;;; The bouncing Squareball

(defparameter *kick-disabled-time* 15)
(defvar *auto-return-distance* 520)
(defparameter *ball-movement-frames* 15)

(defclass ball (thing)
  ((color :initform "white")
   (image :initform "ball.png")
   (slide-frames :initform 4)
   (last-contact :initform nil :accessor last-contact)
   (contact-timer :initform 0 :accessor contact-timer)
   (timer :initform 6)))

(defclass ball-1 (ball) ())
(defclass ball-2 (ball) ())

(defmethod collide :around ((thing thing) (ball ball))
  (when (or (null (last-contact ball))
	    (not (xelf::object-eq thing (last-contact ball))))
    (call-next-method))
  (setf (contact-timer ball) 10)
  (setf (last-contact ball) thing))

(defmethod update :after ((ball ball))
  (when (plusp (contact-timer ball))
    (decf (contact-timer ball)))
  (when (zerop (contact-timer ball))
    (setf (last-contact ball) nil)))

(defmethod bounce ((ball ball))
  (play-sample "bip.wav")
  (with-slots (direction) ball
    (when (sliding-p ball)
      (cancel-slide ball)
      (restore-from-slide ball))
    (setf (slot-value ball 'timer) 0)
    (face ball (opposite-direction direction))))

(defmethod update ((ball ball))
  (with-fields (timer) ball
    (when (plusp timer) 
      (decf timer)))
  (when (not (sliding-p ball))
    (slide-forward ball)))
    
(defmethod arrive ((ball ball))
  (when (not (sliding-p ball))
    (snap-to-grid ball)
    (slide-forward ball)))

(defmethod draw ((ball ball))
  (with-fields (color x y width height) ball
    (draw-box (+ x 3) (+ y 3) (- width 6) (- height 6) :color "white")
    (draw-box (+ x 5) (+ y 5) (- width 10) (- height 10) :color color)))

;;; Colored bricks

(defclass brick (thing)
  ((color :initform "blue")))

(defmethod initialize-instance :after ((brick brick) &key (color "blue"))
  (setf (slot-value brick 'color) 
	(or (pen-color) color)))

(defmethod entry-args ((brick brick))
  (list :color (slot-value brick 'color)))

(defmethod draw ((brick brick))
  (with-fields (grid-x grid-y color) brick
    (multiple-value-bind (top left right bottom)
	(grid-position-bounding-box grid-x grid-y)
      (draw-box left top (- right left) (- bottom top) :color color))))

(defmethod collide ((brick brick) (ball ball))
  (paint ball (find-color brick))
  (bounce ball)
  (destroy brick))

(defmethod draw-bricks (x y width height color)
  (let (bricks)
    (dotimes (i width)
      (dotimes (j height)
	(let ((brick (make-instance 'brick :color color)))
	  (push brick bricks)
	  (add-node (current-buffer) brick)
	  (move-to-grid brick (+ x i) (+ y j)))))
    bricks))

;;; Walls

(defparameter *wall-color* "gray20")

(defclass wall (brick)
  ((color :initform *wall-color*)))

(defmethod draw ((wall wall))
  (with-fields (grid-x grid-y) wall
    (draw-box (* *unit* grid-x) (* *unit* grid-y) *unit* *unit* :color (wall-color))))

(defmethod collide ((ball ball) (wall wall)) nil)
;;  (bounce ball))

(defmethod collide ((wall wall) (ball ball))
  (bounce ball))

;;; Gate bricks

(defclass gate (brick) ())

(defmethod draw :after ((gate gate))
  (with-slots (x y width height) gate
    (draw-image "x.png" x y :width width :height height)))

(defmethod collide :around ((gate gate) (ball ball))
  (if (same-color-p gate ball)
      (call-next-method)
      (progn 
	(play-sample "shield.wav")
	(bounce ball))))

(defmethod destroy :after ((gate gate))
  (when *quadtree*
    (play-sample "worp.wav")))

;;; Doorway bricks

(defclass door (thing) 
  ((timer :initform 0)
   (image :initform "gate-closed.png")))

(defmethod activate ((door door))
  (with-slots (timer image) door
    (setf image "gate-open.png")
    (setf timer 350)))

(defmethod deactivate ((door door))
  (with-slots (image) door
    (setf image "gate-closed.png")))

(defmethod openp ((door door))
  (plusp (slot-value door 'timer)))

(defmethod collide :around ((thing thing) (door door))
  (if (openp door)
      (call-next-method)
      (progn (cancel-slide thing)
	     (restore-from-slide thing))))

(defmethod collide ((ball ball) (door door))
  (bounce ball)
  (activate door))

(defmethod update :after ((door door))
  (with-slots (timer) door
    (when (plusp timer)
      (decf timer)
      (when (zerop timer)
	(deactivate door)))))

;;; Exit

(defparameter *exit-open-images* (image-set "door" 5))
(defparameter *exit-closed-images* (image-set "door-closed" 5))

(defun exit-image (&key (phase xelf:*updates*) (images *exit-open-images*))
  (let ((index (mod (truncate (/ phase 50)) (length images))))
    (nth index images)))

(defclass exit (thing)
  ((image :initform "door.png")))

(defmethod update :before ((exit exit))
  (setf (slot-value exit 'image)
	(if (find-instances (current-buffer) 'enemy)
	    (exit-image :images *exit-closed-images*)
	    (exit-image :images *exit-open-images*))))

;;; Chevrons

(defparameter *max-chevrons* 4)

(defclass chevron (thing)
  ((image :initform "chevron.png")
   (id :initform 0 :accessor id :initarg :id)))

(defmethod initialize-instance :after ((chevron chevron) &key (direction :right))
  (face chevron direction))

(defun find-chevrons ()
  (find-instances (current-buffer) 'chevron))

(defun first-chevron ()
  (first (sort (find-chevrons) #'< :key #'id)))

(defun max-chevrons-p ()
  (= *max-chevrons* (length (find-chevrons))))

(defmethod draw :after ((chevron chevron))
  (let ((f (first-chevron)))
    (when (and (object-eq f chevron) (= 4 (length (find-instances (current-buffer) 'chevron))))
      (multiple-value-bind (top left right bottom) (bounding-box chevron)
	(with-slots (heading) chevron
	  (draw-textured-rectangle-* left top 0 
				     (- right left)
				     (- bottom top)
				     (find-texture "chevron.png")
				     :blend :alpha
				     :opacity 0.5
				     :vertex-color (random-choose '("white" "cyan" "magenta" "yellow"))
				     :angle (+ 90 (heading-degrees heading))))))))

;;; The player avatar

(defparameter *player-1-image* "robot.png")
(defparameter *player-2-image* "robot.png")

(defclass player (thing)
  ((image :initform *player-1-image*)
   (player-id :initform 1)
   (input-direction :initform nil :accessor input-direction)
   (input-fire :initform nil :accessor input-fire)
   (input-deploy :initform nil :accessor input-deploy)
   (deadp :initform nil :accessor deadp)
   (slide-frames :initform 7)
   (throw-timer :initform 0 :accessor throw-timer)
   (paint-color :initform "white" :accessor paint-color)))

(defmethod update :before ((player player))
  (with-slots (grid-x grid-y) player
    (when (and (zerop grid-x) (zerop grid-y))
      (message "Dropping misplaced object ~S" player)
      (move-to-grid player 2 2))))

(defmethod kill ((player player))
  (when (not (deadp player))
    (setf (deadp player) t)
    (play-sample "death.wav")
    (play-sample "death-alien.wav")))

(defmethod paint ((player player) color)
  (setf (paint-color player) color)) 

(defmethod can-throw-p ((player player))
  (zerop (throw-timer player)))

(defmethod update-throw-timer ((player player))
  (with-slots (throw-timer) player
    (when (plusp throw-timer)
      (decf throw-timer))))

(defmethod find-ball ((player player))
  (first (find-instances (current-buffer) 'ball)))

(defmethod make-ball ((player player))
  (set-ball-1 (make-instance 'ball)))

(defmethod throw-ball ((player player))
  (unless (find-ball player)
    (play-sample "serve.wav")
    (with-slots (throw-timer paint-color grid-x grid-y direction) player
      (setf throw-timer 20)
      (let ((ball (make-ball player)))
	(add-node (current-buffer) ball)
	(move-to-grid ball grid-x grid-y)
	(face ball direction)
	(paint ball paint-color)))))

(defmethod grab ((player player) (ball ball))
  (setf (throw-timer player) 20)
  (play-sample "grab.wav")
  (paint player (find-color ball))
  (destroy ball))

(defmethod collide ((player player) (ball ball)) nil)
  ;; (with-slots (timer) ball
  ;;   (when (and (zerop timer)
  ;;     (grab player ball)))))

(defmethod touching-chevron-p ((thing thing))
  (block searching
    (do-nodes (node (current-buffer))
      (when (and (typep node (find-class 'chevron))
		 (colliding-with-p thing node))
	(return-from searching node)))))

(defmethod drop-chevron ((player player))
  (when (not (touching-chevron-p player))
    (with-slots (grid-x grid-y direction) player
      (when (max-chevrons-p)
	(destroy (first-chevron)))
      (let ((chevron (make-instance 'chevron :direction direction :id *updates*)))
	(add-node (current-buffer) chevron)
	(move-to-grid chevron grid-x grid-y)
	(play-sample "buzz.wav")))))

(defmethod collide ((chevron chevron) (ball ball))
  (bounce ball)
  (play-sample "turn.wav")
  (turn-rightward chevron))

(defmethod collide ((brick brick) (player player))
  (cancel-slide player)
  (restore-from-slide player))

(defmethod holding-fire ((player player)) nil)
(defmethod holding-deploy ((player player)) nil)
(defmethod holding-direction ((player player)) nil)

(defmethod update ((player player))
  (update-throw-timer player)
  (unless (sliding-p player)
    (when (holding-deploy player)
      (drop-chevron player))
    (let ((dir (holding-direction player)))
      (when dir
	(face player dir)
	(slide player dir 1))))
  (when (and (holding-fire player) 
	     (can-throw-p player))
    (throw-ball player)))

(defmethod update :around ((player player))
  (unless (deadp player)
    (call-next-method)))

;;; Your goal is to get to the exit

(defmethod collide ((exit exit) (player player))
  (unless (find-instances (current-buffer) 'enemy)
    (next-level (current-buffer))))

;;; First player uses keyboard (or joystick 1)

(defclass player-1 (player) 
  ((color :initform *player-1-color*)
   (player-id :initform 1)))

(defmethod find-special ((player player-1))
  (or (player-1) player))

(defmethod collide :around ((chevron chevron) (ball ball-1))
  (unless (colliding-with-p chevron (player-1))
    (call-next-method)))

(defmethod make-ball ((player player-1))
  (set-ball-1 (make-instance 'ball-1)))

(defmethod find-ball ((player player-1))
  (first (find-instances (current-buffer) 'ball-1)))

(defmethod collide :after ((player player-1) (ball ball-1))
  (with-slots (timer) ball
    (when (and (zerop timer)
      (grab player ball)))))

(defmethod collide ((player player-1) (ball ball-2)) nil)

(defmethod holding-fire ((player player-1))
  (or (holding-shift) (holding-button :throw)))

(defmethod holding-deploy ((player player-1)) 
  (or (holding-space) (holding-button :deploy)))

(defmethod holding-direction ((player player-1))
  (cond ((or (holding-down-arrow) (holding-button :down)) :down)
	((or (holding-up-arrow) (holding-button :up)) :up)
	((or (holding-left-arrow) (holding-button :left)) :left)
	((or (holding-right-arrow) (holding-button :right)) :right)))

;;; Second player uses joystick

(defclass player-2 (player)
  ((color :initform *player-2-color*)
   (player-id :initform 2)))

(defmethod find-special ((player player-2))
  (or (player-2) player))

(defmethod collide :around ((chevron chevron) (ball ball-2))
  (unless (colliding-with-p chevron (player-2))
    (call-next-method)))

(defmethod make-ball ((player player-2))
  (set-ball-2 (make-instance 'ball-2)))

(defmethod find-ball ((player player-2))
  (first (find-instances (current-buffer) 'ball-2)))

(defmethod collide :after ((player player-2) (ball ball-2))
  (with-slots (timer) ball
    (when (and (zerop timer)
      (grab player ball)))))

(defmethod collide ((player player-2) (ball ball-1)) nil)

(defmethod holding-fire ((player player-2))
  (holding-button :throw :joystick *player-2-joystick* :config *player-2-config*))

(defmethod holding-deploy ((player player-2)) 
  (holding-button :deploy :joystick *player-2-joystick* :config *player-2-config*))

(defmethod holding-direction ((player player-2))
  (cond ((holding-button :down :joystick *player-2-joystick* :config *player-2-config*) :down)
	((holding-button :up :joystick *player-2-joystick* :config *player-2-config*) :up)
	((holding-button :left :joystick *player-2-joystick* :config *player-2-config*) :left)
	((holding-button :right :joystick *player-2-joystick* :config *player-2-config*) :right)))

;;; Spawn points

(defmethod spawn ((thing thing)) nil)

(defclass spawn-1 (thing)
  ((image :initform "enemy.png")))

(defmethod spawn ((spawn spawn-1))
  (with-slots (grid-x grid-y) spawn
    (add-node (arena) (player-1))
    (move-to (player-1) grid-x grid-y)
    (destroy spawn)))

(defclass spawn-2 (thing)
  ((image :initform "enemy.png")))

(defmethod spawn ((spawn spawn-2))
  (with-slots (grid-x grid-y) spawn
    (add-node (arena) (player-2))
    (move-to (player-2) grid-x grid-y)
    (destroy spawn)))

(defun player-1-spawn-point () (make-instance 'spawn-1))
(defun player-2-spawn-point () (make-instance 'spawn-2))

;;; Apply shading/detail to the player(s) and show held color

(defmethod draw :after ((player player))
  (with-fields (color heading kick-clock paint-color) player
    (multiple-value-bind (top left right bottom)
	(bounding-box player)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture "robot-detail.png")
				 :vertex-color color
				 :angle (+ 90 (heading-degrees heading)))
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture "robot-swatch.png")
				 :vertex-color paint-color
				 :angle (+ 90 (heading-degrees heading))))))

;;; Draw as skull when game over

(defmethod draw :around ((player player))
  (if (deadp player)
      (with-slots (x y height width) player
	(draw-image "skull.png" (- x (units 0.5)) (- y (units 0.5)) :width (+ width (units 1)) :height (+ height (units 1))))
      (call-next-method)))

;;; Can't go thru walls

(defmethod collide ((wall wall) (player player))
  (cancel-slide player)
  (restore-from-slide player))

;;; Enemy base class

(defclass enemy (thing)
  ((image :initform "enemy.png")
   (slide-frames :initform (by-level 80 75 70 65 60))
   (stun-timer :initform 0 :accessor stun-timer)))

(defmethod cursor ((enemy enemy))
  (when (player-1)
    (if (player-2)
	(let ((distance-1 (distance-between enemy (player-1)))
	      (distance-2 (distance-between enemy (player-2))))
	  (if (> distance-1 distance-2)
	      (player-2)
	      (player-1)))
	(player-1))))

(defmethod update :around ((enemy enemy))
  (when (plusp (stun-timer enemy))
    (decf (stun-timer enemy)))
  (when (not (stunnedp enemy))
    (call-next-method)))

(defmethod stunnedp ((enemy enemy))
  (plusp (stun-timer enemy)))

(defmethod stun ((enemy enemy) &optional (frames 350))
  ;;(play-sample "upwoop.wav")
  (setf (stun-timer enemy) frames))

(defmethod collide ((enemy enemy) (ball ball))
  (bounce ball)
  (unless (stunnedp enemy)
    (stun enemy)))

(defmethod collide ((enemy enemy) (player player))
  (kill player))

(defmethod collide ((enemy enemy) (thing thing))
  (cancel-slide enemy)
  (restore-from-slide enemy))

(defmethod think ((enemy enemy))
  (percent-of-time 1 (face enemy (random-choose *grid-directions*)))
  (percent-of-time 1 (slide-forward enemy)))

;;; Enemies follow chevrons

(defmethod collide ((enemy enemy) (chevron chevron)) nil)

(defmethod arrive :before ((enemy enemy))
  (let ((chevron (touching-chevron-p enemy)))
    (when chevron
      (play-sample "chevron.wav")
      (face enemy (slot-value chevron 'direction)))))

;;; Shockers

(defclass shocker (enemy)
  ((image :initform "shocker.png")
   (slide-frames :initform (by-level 50 40 35 30))))

(defmethod initialize-instance :after ((shocker shocker) &key)
  (setf (slot-value shocker 'direction)
	(random-choose '(:up :down :upleft :downright))))

(defmethod think :around ((shocker shocker))
  (if (< (distance-to-cursor shocker) 100)
      (progn 
	(face shocker (direction-to-cursor shocker))
	(slide-forward shocker))
      (slide-forward shocker)))

(defmethod collide :after ((shocker shocker) (wall wall))
  (stun shocker 2)
  (percent-of-time 50 (face shocker (opposite-direction (slot-value shocker 'direction)))))
      
;;; Monitors 

(defclass monitor (enemy)
  ((image :initform "monitor.png")
   (slide-frames :initform (by-level 45 40 35 30 25))))

(defmethod collide :after ((monitor monitor) (enemy enemy))
  (percent-of-time 10 
    (cancel-slide monitor) 
    (restore-from-slide monitor)
    (turn-leftward monitor)))

(defmethod chase ((monitor monitor))
  (slide monitor (direction-to-cursor monitor))
  (play-sample "shield-warning.wav")
  (stun monitor 60))
  
(defmethod think ((monitor monitor))
  (if (< (distance-to-cursor monitor) 120)
      (chase monitor)
      (slide-forward monitor)))

(defmethod stun :after ((monitor monitor) &optional frames)
  (when (< (distance-to-cursor monitor) (units 3.6))
    (kill (cursor monitor))))

(defmethod collide :after ((monitor monitor) (wall wall))
  (unless (touching-chevron-p monitor) (stun monitor 20))
  (face monitor (random-choose '(:up :down :left :right :upright :downleft))))

(defmethod collide :after ((monitor monitor) (brick brick))
  (unless (touching-chevron-p monitor) (stun monitor 20))
  (face monitor (random-choose '(:up :down :left :right :upright :downleft))))

(defmethod draw :after ((monitor monitor))
  (when (stunnedp monitor)
    (multiple-value-bind (top left right bottom) (bounding-box monitor)
      (draw-image "ylod.png" (- left (units 2)) (- top (units 2)) 
		  :width (- right left (units -4))
		  :height (- bottom top (units -4))))))

;;; Paddles

(defparameter *paddle-images* (image-set "paddle" 3))
(defun random-paddle-image () (random-choose *paddle-images*))

(defclass paddle (enemy)
  ((image :initform (random-paddle-image))
   (slide-frames :initform (by-level 50 45 45 40 40 35 35 30 30))))

(defmethod collide :after ((paddle paddle) (enemy enemy))
  (percent-of-time 10 
    (cancel-slide paddle) 
    (restore-from-slide paddle)
    (turn-around paddle)))

(defmethod collide :after ((paddle paddle) (thing thing))
  (unless (touching-chevron-p paddle)
    (cancel-slide paddle) 
    (restore-from-slide paddle)
    (turn-around paddle)))

(defmethod think ((paddle paddle))
  (slide-forward paddle))

(defmethod update :after ((paddle paddle))
  (setf (slot-value paddle 'image) (random-paddle-image)))

;;; Wires

(defclass wire (thing)
  ((image :initform "wire.png")
   (timer :initform (by-level 300 350 400 450 500 550 600 660 700) :accessor timer)))

(defmethod collide ((wire wire) (player player))
  (kill player))

(defmethod update :after ((wire wire))
  (decf (timer wire))
  (unless (plusp (timer wire))
    (destroy wire)))

;;; Tracers

(defclass tracer (enemy)
  ((image :initform "tracer.png")
   (slide-frames :initform (by-level 31 29 27 25 23 21))))

(defmethod initialize-instance :after ((tracer tracer) &key)
  (face tracer (random-choose 
		(by-level '(:up :down :left :right)
			  '(:up :down :left :right)
			  '(:up :down :left :right)
			  '(:up :down :left :right)
			  '(:upleft :downright :downleft :upleft)
			  '(:upleft :downright :downleft :upleft)
			  '(:upleft :downright :downleft :upleft :up :down :left :right)))))

(defmethod think ((tracer tracer))
  (with-slots (grid-x grid-y direction) tracer
    ;;; check for too many wires
    (let ((wires 0))
      (do-nodes (node (current-buffer))
	(when (and (typep node (find-class 'wire))
		   (colliding-with-p node tracer))
	  (incf wires)))
      (when (< wires 4)
	(let ((wire (make-instance 'wire)))
	  (add-node (current-buffer) wire)
	  (move-to-grid wire grid-x grid-y)
	  (face wire direction))))
    (slide-forward tracer)))

(defmethod collide :after ((tracer tracer) (brick brick))
  (if (percent-of-time 90 t)
      (turn-rightward tracer)
      (turn-leftward tracer)))

(defmethod collide :after ((tracer tracer) (enemy enemy))
  (percent-of-time 10 
    (cancel-slide tracer) 
    (restore-from-slide tracer)
    (turn-leftward tracer)))

(defmethod collide ((tracer tracer) (wire wire)) nil)
(defmethod collide ((wire wire) (tracer tracer)) nil)

;;; Black holes 

(defclass hole (thing)
  ((image :initform "hole.png")
   (fullp :initform nil :accessor fullp)))

(defmethod pack ((hole hole))
  (play-sample "hole.wav")
  (setf (slot-value hole 'image) "hole-closed.png")
  (setf (fullp hole) t))

(defmethod collide ((hole hole) (enemy enemy))
  (when (not (fullp hole))
    (destroy enemy)
    (play-sample "hole.wav")
    (destroy hole)))

(defmethod collide ((hole hole) (player player))
  (when (not (fullp hole))
    (kill player)
    (pack hole)))

;;; Level generation utilities.

(defclass entry ()
  ((x :initform 0 :initarg :x)
   (y :initform 0 :initarg :y)
   (class :initform nil :initarg :class)
   (args :initform nil :initarg :args)))

(defmethod entry-args ((node node)) nil)

(defmethod find-entry ((node node))
  (with-slots (grid-x grid-y) node
    (make-instance 'entry
		   :x grid-x :y grid-y 
		   :class (class-name (class-of node))
		   :args (entry-args node))))

(defmethod expand ((entry entry))
  (with-slots (x y class args) entry
    (let ((node (apply #'make-instance (find-class class) args)))
      (with-buffer (arena)
	(let ((output-node (or (find-special node) node)))
	  (add-node (arena) output-node)
	  (move-to-grid output-node x y)
	  output-node)))))

(defun buffer-entries (buffer)
  (mapcar #'find-entry (get-nodes buffer)))

(defun collapse-buffer (buffer)
  (buffer-entries buffer))
;; (destroy buffer)))

(defmacro with-entries (buffer &body body)
  (let ((x (gensym)))
    `(let ((,x ,buffer))
       (with-buffer ,x 
	 ,@body
	 (collapse-buffer ,x)))))

(defmethod transform ((entry entry) dx dy)
  (with-slots (x y) entry
    (incf x dx)
    (incf y dy)
    entry))
 
(defun transform-entries (entries dx dy)
  (flet ((tx (entry) (transform entry dx dy)))
    (mapcar #'tx entries)))

(defun find-height (entries)
  (if (null entries) 
      0
      (flet ((find-y (thing) (slot-value thing 'y)))
	(1+ (reduce #'max (mapcar #'find-y entries))))))

(defun find-width (entries)
  (if (null entries) 
      0
      (flet ((find-x (thing) (slot-value thing 'x)))
	(1+ (reduce #'max (mapcar #'find-x entries))))))

(defun merge-entries (a b)
  (append a b))

(defun place-beside (a b &optional (padding 0))
  (merge-entries a (transform-entries b (+ padding (find-width a)) 0)))

(defun place-below (a b &optional (padding 0))
  (merge-entries a (transform-entries b 0 (+ padding (find-height a)))))

(defparameter *puzzle-padding* (units 2))

(defun pad-entries (a &key (x *puzzle-padding*) (y *puzzle-padding*))
  (transform a x y))

(defun horizontally (a b &optional (pad *puzzle-padding*))
  (percent-of-time 50 (rotatef a b))
  (place-beside 
   (pad-entries a :x pad :y pad)
   (pad-entries b :x pad :y pad)))

(defun vertically (a b &optional (pad *puzzle-padding*))
  (percent-of-time 50 (rotatef a b))
  (place-below 
   (pad-entries a :x pad :y pad)
   (pad-entries b :x pad :y pad)))

(defun either-way (a b)
  (funcall (or (percent-of-time 50 #'horizontally) #'vertically)
	   a b))

(defun singleton (x) 
  (find-entry x))

(defun random-color () (random-choose (theme-colors)))

(defvar *pen-color* nil)
(defun pen-color () *pen-color*)
(defun set-pen-color (color) (setf *pen-color* color))
(defvar *required-color* nil)

(defmacro with-pen-color (color &body forms)
  `(let ((*pen-color* ,color)) ,@forms))

(defmacro requiring-color (color &body forms)
  `(let ((*required-color* ,color)) ,@forms))

(defun level-padding ()
  (by-level 5));; 5 5 4 4 4 3 3 3 2 2 2 1))

(defun stacked-up (&rest things)
  (assert things)
  (if (= 1 (length things))
      (first things)
      (place-below (first things) (apply #'stacked-up (rest things)) (level-padding))))

(defun lined-up (&rest things)
  (assert things)
  (if (= 1 (length things))
      (first things)
      (place-beside (first things) (apply #'lined-up (rest things)) (level-padding))))

;;; The arena

(defclass arena (xelf:buffer)
  ((quadtree-depth :initform 9)))

(defmethod grid-offset ((arena arena)) *margin-width*)

(defmethod grid-object-size ((arena arena))
  (- (units 1) (* 2 *margin-width*)))

(defun drop-wall (gx gy)
  (let ((wall (make-instance 'wall)))
    (add-node (current-buffer) wall)
    (move-to-grid wall gx gy)))

(defun draw-horizontal-wall (gx gy length)
  (dotimes (n length)
    (drop-wall (+ n gx) gy)))

(defun draw-vertical-wall (gx gy length)
  (dotimes (n length)
    (drop-wall gx (+ n gy))))

(defun draw-border-wall (gx gy width height)
  (draw-horizontal-wall 0 0 width)
  (draw-horizontal-wall 0 (- height 1) width)
  (draw-vertical-wall 0 1 (- height 2))
  (draw-vertical-wall (- width 1) 1 (- height 2)))

(defun draw-courtyard (gx gy width height &optional shaft nogate)
  (let* ((lenx (truncate (/ width 2)))
	 (leny (truncate (/ height 2)))
	 (cx (+ gx lenx))
	 (cy (+ gy leny)))
    (draw-horizontal-wall gx gy (- cx gx 1))
    (draw-horizontal-wall cx gy (- cx gx -1))
    (when shaft
      (draw-vertical-wall (- cx 2) (+ 1 gy) 3)
      (draw-vertical-wall cx (+ 1 gy) 3))
    (percent-of-time (by-level 0 10 20 30 40)
      (place (random-enemy-not-tracer) (+ gx 5) (+ gy 3)))
    (place (make-instance 'wall) (- cx 2) (+ gy height -2))
    (unless nogate
      (percent-of-time (by-level 0 10 20 30 40)
	(place (make-instance 'gate :color (random-choose (theme-colors))) (- cx 1) gy)))
    (draw-horizontal-wall gx (+ gy height -1) width)
    (draw-vertical-wall gx (1+ gy) (- height 2))
    (draw-vertical-wall (+ -1 gx width) (1+ gy) (- height 2))))

(defun turnstile (gx gy) 
  (draw-horizontal-wall (+ gx 1) (+ gy 3) 3)
  (draw-horizontal-wall (+ gx 3) (+ gy 3) 3)
  (draw-vertical-wall (+ gx 3) (+ gy 1) 2)
  (draw-vertical-wall (+ gx 3) (+ gy 4) 2))
  
(defmethod quit-game ((arena arena))
  (quit))

;;; Puzzle generation

(defun width-in-units () (truncate (/ *width* *unit*)))
(defun height-in-units () (truncate (/ *height* *unit*)))

(defun level-enemies ()
  (by-level '(tracer) 
	    '(tracer monitor) 
	    '(tracer shocker) 
	    '(shocker monitor) 
	    '(shocker tracer monitor)
	    '(paddle tracer)
	    '(paddle monitor)
	    '(paddle shocker)
	    '(paddle tracer shocker)
	    '(shocker paddle monitor)))

(defun random-enemy ()
  (make-instance (find-class (random-choose (level-enemies)))))

(defun random-enemy-not-tracer ()
  (make-instance (find-class (random-choose (remove 'tracer (level-enemies))))))

(defun num-enemies ()
  (by-level 2 2 2 3 3 3 4 4 4))
  
(defun draw-bricks-with-hole (x y width height &optional color)
  (let ((bricks (draw-bricks x y width height color)))
    (let ((hx (+ 1 x (random (- (max width 3) 2))))
	  (hy (+ 1 y (random (- (max height 3) 2)))))
      (dolist (brick bricks)
	(with-slots (grid-x grid-y) brick
	  (when (and (= hx grid-x)
		     (= hy grid-y))
	    (destroy brick)
	    (place (make-instance 'hole) hx hy)))))))

(defparameter *level-templates*
  '(((entrance third-color lone-enemy first-color second-color) 
     (turnstile pair-of-enemies fortress second-color second-color)
     (courtyard first-color turnstile first-color third-color))
    ;;
    ((entrance third-color lone-enemy first-color second-color) 
     (turnstile pair-of-enemies fortress second-color second-color)
     (courtyard first-color lone-enemy first-color third-color))
    ;;
    ((entrance third-color lone-enemy first-color second-color) 
     (turnstile pair-of-enemies fortress second-color second-color)
     (courtyard first-color lone-enemy first-color third-color))
    ;;
    ((entrance third-color lone-enemy first-color second-color) 
     (turnstile pair-of-enemies fortress second-color second-color)
     (courtyard first-color lone-enemy first-color third-color))))
  
(defun level-template-number ()
  (by-level 0 1 1 1 1 1 2 2 2 3))

(defun find-level-template ()
  (nth (level-template-number) *level-templates*))

(defun make-level-template (&optional (level *level*))
  (derange (mapcar #'derange (find-level-template))))

(defun entrance (x y)
  (add-node (current-buffer) (player-1))
  (move-to-grid (player-1) (+ 2 x) (+ 2 y))
  (face (player-1) :right)
  (when (coop-p)
    (add-node (current-buffer) (player-2))
    (move-to-grid (player-2) (+ 4 x) (+ 2 y))
    (face (player-2) :right)))

(defun lone-enemy (x y)
  (place (random-enemy) x y))

(defun pair-of-enemies (x y)
  (let ((enemy (random-enemy)))
    (percent-of-time (by-level 0 10 20 30 40 60)
      (place enemy x y))
    (percent-of-time (by-level 0 10 20 30 40 50)
      (place (make-instance (class-of enemy)) (+ x 2) y))))

(defun first-color (x y)
  (draw-bricks-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (first (theme-colors))))

(defun second-color (x y)
  (draw-bricks-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (second (theme-colors))))

(defun third-color (x y)
  (draw-bricks-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (third (theme-colors))))

(defun fourth-color (x y)
  (draw-bricks-with-hole x y (+ 6 (random 6)) (+ 4 (random 5)) (fourth (theme-colors))))

(defun courtyard (x y)
  (draw-courtyard x y 7 7))

(defun fortress (x y)
  (draw-courtyard x y 9 7 t :nogate)
  (let ((colors (derange (theme-colors))))
    (place (make-instance 'gate :color (pop colors)) (+ x 3) y)
    (place (make-instance 'gate :color (pop colors)) (+ x 3) (+ 1 y))
    (place (make-instance 'gate :color (pop colors)) (+ x 3) (+ 2 y)))
  (place (make-instance 'exit) (+ x 1) (+ y 1)))

(defun item-entries (item)
  (assert (not (null item)))
  (assert (symbolp item))
  (assert (fboundp item))
  (let (entries)
    (with-new-buffer 
      (setf entries (with-entries (current-buffer) (funcall item 0 0))))
    entries))

(defun expand-entries (entries)
  (mapcar #'expand entries))

(defun row-entries (row)
  (apply #'lined-up (mapcar #'item-entries row)))

(defun template-entries (template)
  (apply #'stacked-up (mapcar #'row-entries template)))

(defun find-arena-class (&optional netplay)
  (case netplay
    (:client 'client-arena)
    (:server 'server-arena)
    (otherwise 'arena)))

(defun make-game (&optional netplay)
  (with-buffer (make-instance (find-arena-class netplay))
    (multiple-value-bind (x y) (center-point (current-buffer))
      ;;(populate (current-buffer))
      (current-buffer))))

(defmethod initialize-instance :after ((arena arena) &key)
  (with-buffer arena
    (setf *arena* arena)
    (resize arena *width* *height*)
    (bind-event arena '(:escape) 'setup)
    (bind-event arena '(:r :control) 'reset-game)
    (bind-event arena '(:y :control) 'retry-level)
    (bind-event arena '(:p :control) 'pause)
    (bind-event arena '(:n :control) 'next-level)
    (bind-event arena '(:q :control) 'quit-game)
    (draw-border-wall 0 0 (width-in-units) (height-in-units))
    (place (make-instance 'wall) (+ 6 (random 25)) 1)
    (place (make-instance 'wall) (+ 12 (random 25)) 40)
    (place (make-instance 'wall) (+ 40 (random 13)) 40)
    (set-player-1 (make-instance 'player-1))
    (when (coop-p)
      (set-player-2 (make-instance 'player-2)))
    (let ((template (make-level-template)))
      (expand-entries 
       (transform-entries 
	(template-entries template)
	4 4)))
    (do-nodes (node arena)
      (spawn node))))

(defun do-reset ()
  (dotimes (n 64) 
    (halt-sample n))
  (play-sample "go.wav")
  (play-sample (random-choose '("fanfare-1.wav" "fanfare-2.wav" "fanfare-3.wav")))
  (reset-score)
  (set-theme (random-choose (mapcar #'car (append *three-brick-themes* *four-brick-themes*))))
  (when (not (coop-p))
    (set-player-2 nil))
  (start (make-game *netplay*))
  (reset-game-clock))

(defmethod reset-game ((arena arena))
  (stop arena)
  (setf *level* 0)
  (do-reset)
  (at-next-update (destroy arena)))

(defmethod next-level ((arena arena))
  (stop arena)
  (incf *level*)
  (do-reset)
  (at-next-update (destroy arena)))

(defmethod retry-level ((arena arena))
  (stop arena)
  (do-reset)
  (at-next-update (destroy arena)))

(defmethod pause ((arena arena))
  (setf *paused* (if *paused* nil t)))

(defmethod update :around ((arena arena))
  (when (not *paused*)
    (update-game-clock)
    (call-next-method)))

(defmethod draw :before ((arena arena))
  (draw-box 0 0 *width* *height* :color (background-color)))

(defun status-line-string ()
  (if (not *paused*)
      "[Arrows] Move  [Shift] Throw  [Space] Deploy  [Ctrl-R] reset  [Ctrl-Y] retry level  [Ctrl-N] next level  [Ctrl-P] pause  [Ctrl-Q] quit  [Esc] Setup "
      "Game is paused. Press [Control-P] again to resume game, or [Control-Q] to quit."))

(defmethod draw :after ((arena arena))
  (draw-box 0 (- *height* 20) *width* 20 :color "gray20")
  (let ((color (paint-color (player-1))))
    (when (not *paused*)
      (draw-string (format nil " Level ~S" (1+ *level*)) 
		   (units 7.8) (- *height* 17)
		   :color "yellow"
		   :font *score-font*)
      (draw-box (units 0.2) (- *height* 16) 22 12 :color color)
      (draw-string (string-capitalize color)
		   (units 2.1) (- *height* 17)
		   :color color
		   :font *score-font*)
      (draw-string (game-clock-string) 
		   (units 11.9) (- *height* 17)
		   :color "cyan"
		   :font *score-font*))
    (draw-string (status-line-string)
		 (units 14.2) (- *height* 17)
		 :color "white"
		 :font *score-font*))
  (do-nodes (node arena)
    (when (or (typep node (find-class 'enemy))
	      (typep node (find-class 'player)))
      (draw node))))

;;; Preventing mousey movements

(defmethod handle-point-motion ((self arena) x y))
(defmethod press ((self arena) x y &optional button))
(defmethod release ((self arena) x y &optional button))
(defmethod tap ((self arena) x y))
(defmethod alternate-tap ((self arena) x y))

;;; Setup screen

(defparameter *button-time* 30)
(defparameter *ready-time* 120)

(defclass setup (xelf:buffer)
  ((timer :initform 0)
   (player :initform 1)
   (button :initform nil)
   (quadtree-depth :initform 4)
   (background-color :initform "CornflowerBlue")))

(defmethod initialize-instance :after ((setup setup) &key)
  (hide-terminal)
  (setf *player-1-joystick* nil *player-2-joystick* nil)
  (resize setup *width* *height*))

(defmethod update :after ((setup setup))
  (with-fields (timer player) setup
    (when (plusp timer) 
      (decf timer))
    (when (and (zerop timer)
	       (null player))
      (stop setup)
      (do-reset))))

(defparameter *controller-buttons*
  '(:throw "THROW BALL"
     :deploy "DEPLOY CHEVRON"
     :up "MOVE UP"
     :down "MOVE DOWN"
     :left "MOVE LEFT"
     :right "MOVE RIGHT"))

(defparameter *prompt-any-1* "Press any button on Controller 1 to configure buttons.")
(defparameter *prompt-any-2a* "Press any button on Controller 2 to set up co-op play.")
(defparameter *prompt-any-2b* " or, press any button on Controller 1 again to play solo.")
(defparameter *prompt-format-string* "Player ~S, Press the button you wish to use for ~A:")
(defparameter *must* "(Controllers must be plugged in before the application is started.)")

(defparameter *buttons-per-controller* 6)
(defparameter *controller-button-names* '(:up :down :left :right :throw :deploy))

(defun format-button-prompt (player button)
  (let ((command (nth button *controller-button-names*)))
    (format nil *prompt-format-string* player (getf *controller-buttons* command))))

(defun ready-prompt ()
  (if (null *player-2-joystick*)
      (if (null *player-1-joystick*)
	  "Player 1 on Keyboard. Get Ready!"
	  "Player 1 on Controller. Get Ready!")
      (if (null *player-1-joystick*)
	  "Player 1 on Keyboard, Player 2 on Controller. Get Ready!"
	  "Player 1 and Player 2 on Controllers. Get Ready!")))

(defun flicker () (random-choose '("white" "cyan")))

(defmethod draw :after ((setup setup))
  (with-fields (timer player button) setup
    (draw-string "Game setup" (units 28.3) (units 2) :color "white" :font "sans-mono-bold-22")
    (draw-string *must* (units 20) (units 5) :color "white" :font "sans-mono-bold-12")
    (if (numberp button)
	(draw-string (format-button-prompt player button) (units 12) (units 10) :color (flicker) :font *big-font*)
	(case player 
	  (1 (draw-string *prompt-any-1* (units 12) (units 12) :color (flicker) :font *big-font*))
	  (2 (draw-string *prompt-any-2a* (units 12) (units 14) :color (flicker) :font *big-font*)
	   (draw-string *prompt-any-2b* (units 12) (units 16) :color "white" :font *big-font*))))
    (when (null player)
      (draw-string (ready-prompt) (units 12) (units 18) :color "white" :font *big-font*))))

(defmethod handle-event ((setup setup) event)
  (with-fields (timer player button) setup
    (when (and (eql player 1) 
	       (zerop timer)
	       (listp (first event)))
      ;; not a joystick event
      (setf player nil) 
      (setf *player-1-joystick* nil)
      (setf *player-2-joystick* nil)
      (setf *coop-p* nil))
    (when (and (eq :joystick (first event))
	       (not (plusp timer)))
      (destructuring-bind (which new-button dir) (rest event)
	(if (numberp button)
	    (let ((button-name (nth button *controller-button-names*)))
	      ;; (message "Config button ~S new-button ~S" button-name new-button)
	      (setf timer *button-time*)
	      (if (eql player 1) 
		  (setf (getf *player-1-config* button-name) new-button)
		  (setf (getf *player-2-config* button-name) new-button))
	      ;; (message "   new config: B~S P~S ~S " button player (case player (1 *player-1-config*) (2 *player-2-config*)))
	      (incf button)
	      ;; quit if finished
	      (when (>= button 6)
		(setf player (if (eql 1 player) 2 nil))
		(setf button nil) (setf timer *button-time*)))
	    (case player
	      (1 (setf *player-1-joystick* which)
	         (setf timer *button-time*)
	         (setf button 0))
	      (2 
	       (if (eql which *player-1-joystick*)
	       ;; player chose solo play
		   (progn 
		     (setf *coop-p* nil)
		     (setf *player-2-joystick* nil)
		     (setf timer *ready-time*)
		     (setf player nil))
		   (progn 
		     ;; set up player 2
		     (setf *coop-p* t)
		     (setf *player-2-joystick* which)
		     (setf timer *ready-time*)
		     (setf button 0))))))))))

(defmethod setup ((arena arena))
  (stop arena)
  (at-next-update 
    (switch-to-buffer (make-instance 'setup))
    (destroy arena)))

(defmethod handle-point-motion ((self setup) x y))
(defmethod press ((self setup) x y &optional button))
(defmethod release ((self setup) x y &optional button))
(defmethod tap ((self setup) x y))
(defmethod alternate-tap ((self setup) x y))

;;; Netplay integration

(setf *game-variables* 
      '(*updates* *coop-p* *paused* *score-1* *score-2* *game-clock* 
	*level* *difficulty* *theme*))

(setf *object-variables*
      '(*player-1* *player-2* *ball-1* *ball-2*))

(setf xelf:*safe-variables* 
      (append *game-variables* 
	      *object-variables* 
	      xelf:*other-variables*))

(setf xelf:*terminal-bottom* (- *height* (units 1.5)))

(defmethod find-player ((arena arena) n)
  (ecase n
    (1 (player-1))
    (2 (player-2))))

(defmethod find-input ((player player))
  (list :fire (holding-fire player)
	:deploy (holding-deploy player)
	:direction (holding-direction player)))

(defmethod update-input-state :after ((player player) plist)
  (destructuring-bind (&key fire deploy direction) plist
    (setf (input-direction player) direction)
    (setf (input-fire player) fire)
    (setf (input-deploy player) deploy)))

(defmethod holding-fire :around ((player player))
  (if (input-p player)
      (input-fire player)
      (call-next-method)))

(defmethod holding-deploy :around ((player player))
  (if (input-p player)
      (input-deploy player)
      (call-next-method)))

(defmethod holding-direction :around ((player player))
  (if (input-p player)
      (input-direction player)
      (call-next-method)))

(defclass client-arena (client-buffer arena) ())
(defclass server-arena (server-buffer arena) ())

(defmethod background-stream ((arena server-arena))
  (mapc #'(lambda (x) (slot-value x 'xelf::uuid))
	(find-instances arena 'brick)))

(defmethod ambient-stream ((arena server-arena))
  (let ((stream (copy-tree (make-node-stream))))
    (dolist (var *object-variables*)
      (let ((thing (symbol-value var)))
	(when (find thing stream :test #'object-eq)
	  (setf stream (delete (slot-value thing 'xelf::uuid)
			       stream)))))
    stream))

;;; Main program

(defparameter *title-string* "1x0ng 0.7")

(defun 1x0ng (&key 
		(level 1) 
		(netplay *netplay*)
		(server-host *server-host*) 
		(client-host *client-host*)
		(base-port *base-port*)
		verbose-logging)
  (set-theme (random-choose (mapcar #'car *three-brick-themes*)))
  (when netplay 
    (close-netplay))
  (setf *degrade-stream-p* nil)
  (setf *server* nil)
  (setf *client* nil)
  (setf *verbose-p* verbose-logging)
  (setf *server-port* (or base-port *base-port*))
  (setf *client-port* (1+ *server-port*))
  (setf *server-output-port* (+ 2 *server-port*))
  (setf *client-output-port* (+ 3 *server-port*))
  (setf *netplay* netplay)
  (setf *server-host* server-host)
  (setf *client-host* client-host)
  (setf *sent-messages-count* 0)
  (setf *received-messages-count* 0)
  (setf *remote-host* nil)
  (setf *remote-port* nil)
  (setf *last-message-timestamp* 0)
  (setf *flag-received-p* nil)
  (setf *flag-sent-p* nil)
  (reset-game-clock)
  (setf *player-1-joystick* nil)
  (setf *player-2-joystick* nil)
  (setf *coop-p* nil)
  (setf *paused* nil)
  (setf *level* (1- level))
  (setf *fullscreen* nil)
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :linear)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *inhibit-splash-screen* nil)
  (setf *frame-rate* 60)
  (disable-key-repeat) 
  (with-session 
    (open-project "1x0ng")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (play-sample "fanfare-1.wav")
    (play-sample "go.wav")
    (dolist (line (split-string-on-lines *1x0ng-copyright-notice*))
      (logging line))
    (show-terminal)
    (unless *inhibit-splash-screen*
      (logging "PRESS [ESCAPE] KEY TO CONFIGURE PLAYERS, CONTROLLERS, AND NETWORKING."))
    (at-next-update (switch-to-buffer (make-game netplay)))))

;;; 1x0ng.lisp ends here
